import os
from programs.custom_functions.excel_merge import JeanMerge


def main():
    while True:
        print('Jean,歡迎使用Excel合併系統')
        print('請貼上需合併檔案所在資料夾路徑')
        folder_path = input()
        if not os.path.isdir(folder_path):
            print('輸入字串非合法路徑，請重新執行...')
            continue
        print('請輸入表頭位置（整數）。例：表頭在第一行輸入1')
        try:
            header = int(input())
        except ValueError:
            input('請輸入數字....')
            continue
        print('執行類別：\n1 : Excel合併\n2 : BOM合併')
        run_type = input()
        if run_type == '1' or run_type == '2':
            run_type = int(run_type)
        else:
            input('請不要亂打...')
            continue
        jm = JeanMerge(folder_path)
        jm.run(header=header, run_type=run_type)
        want_leave = input('是否需要離開(Y/n):')
        if want_leave.lower() == 'y':
            input('感謝使用，再見～')
            break
        else:
            print('好的，繼續使用～')
            continue


# def test():
#     jm = JeanMerge('/home/sinwei/文件/jeanTest')
#     jm.run()


if __name__ == '__main__':
    main()

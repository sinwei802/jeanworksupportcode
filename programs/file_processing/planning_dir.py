import os
import shutil
from os.path import isfile, join


class PlanningDir(object):
    def __init__(self, path: str):
        self.folder_path = path

    def file_list(self, whole_path=False) -> list:
        _file_list = []
        for root, dirs, files in os.walk(self.folder_path):
            for name in files:
                file_path = join(root, name)
                if isfile(file_path):
                    if whole_path:
                        _file_list.append(file_path)
                    else:
                        _file_list.append(name)

        return _file_list

    def copy_file_by_name(self, search_list: list, target_dir: str):
        for root, dirs, files in os.walk(self.folder_path):
            for name in files:
                if '.'.join(name.split('.')[:-1]) in search_list:
                    if os.path.isfile(os.path.join(target_dir, name)):
                        os.remove(os.path.join(target_dir, name))
                    if not os.path.isdir(target_dir):
                        os.mkdir(target_dir)
                    shutil.copy(os.path.join(root, name), target_dir)
                    print(os.path.join(root, name) + '  符合！')
                else:
                    print(os.path.join(root, name) + '  不符合！')

    def move_file_by_company(self, company_no: str, target_dir: str):
        _file_list = self.file_list()
        for per_file in _file_list:
            if per_file.startswith(company_no):
                if os.path.isfile(os.path.join(target_dir, per_file)):
                    os.remove(os.path.join(target_dir, per_file))
                if not os.path.isdir(target_dir):
                    os.mkdir(target_dir)
                shutil.move(os.path.join(self.folder_path, per_file), target_dir)

    def change_path(self, new_path):
        self.folder_path = new_path

    def move_everything(self, new_path: str):
        _file_list = self.file_list(whole_path=True)
        for file_path in _file_list:
            if os.path.basename(file_path) == 'Thumbs.db':
                pass
            else:
                shutil.copy2(file_path, new_path)
            os.remove(file_path)

    def upload_company_list(self):
        _file_list = self.file_list()
        company_list = []
        for per_file in _file_list:
            company_list.append(per_file[0:8])
        result_list = list(set(company_list))
        result_list.sort(key=company_list.index)
        return result_list

    def del_folder_files(self):
        for root, folders, files in os.walk(self.folder_path):
            for folder in folders:
                work_folder = os.path.join(root, folder)
                print('刪除' + work_folder)
                shutil.rmtree(work_folder, ignore_errors=True)
        for root, folders, files in os.walk(self.folder_path):
            for work_file_name in files:
                work_file = os.path.join(root, work_file_name)
                print('刪除' + work_file)
                os.remove(work_file)


if __name__ == '__main__':
    fp = PlanningDir('/home/sinwei/文件/savemoney/TA')
    fp.copy_file_by_name('01074303', '/home/sinwei/文件')

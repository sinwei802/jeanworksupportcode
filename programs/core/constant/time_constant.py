import datetime


def time_mark_now():
    dt_now = datetime.datetime.now()
    return dt_now.strftime('%Y-%m-%d_%H-%M-%S')


if __name__ == '__main__':
    pass

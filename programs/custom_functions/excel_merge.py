import os
import pandas
import openpyxl
import xlrd
from programs.core.constant import time_constant
from programs.file_processing.planning_dir import PlanningDir


class JeanMerge(object):
    def __init__(self, origin_dir):
        self.temp_dir_ctl = PlanningDir(os.path.join('.', 'temp'))
        self.origin_dir_ctl = PlanningDir(origin_dir)

    def run(self, header=2, run_type=1):
        todo_list = self.__load_todo_list()
        self.origin_dir_ctl.copy_file_by_name(todo_list, self.temp_dir_ctl.folder_path)
        if run_type == 1:
            data_df = pandas.DataFrame(columns=['母件名稱'])
        elif run_type == 2:
            data_df = pandas.DataFrame(columns=['母件編號', '母件名稱', '母件類別'])
        else:
            data_df = pandas.DataFrame(columns=['母件編號', '母件名稱', '母件類別'])
        for per_file in self.temp_dir_ctl.file_list(whole_path=True):
            if str(per_file).endswith('xls') or str(per_file).endswith('xlsx'):
                print(os.path.abspath(per_file))
                mother_item = str(self.get_sheet1_a1(per_file)).split('-')
                temp_df = pandas.read_excel(per_file, header=header - 1)
                if run_type == 1:
                    temp_df['母件名稱'] = '-'.join(os.path.basename(per_file).split('.')[:-1])
                elif run_type == 2:
                    temp_df['母件編號'] = '-'.join(mother_item[0:-2])
                    temp_df['母件名稱'] = mother_item[-2]
                    temp_df['母件類別'] = mother_item[-1]
                data_df = data_df.append(temp_df)
        result_name = time_constant.time_mark_now() + '.xlsx'
        data_df.to_excel(result_name, index=False)
        print('執行完畢，清除暫存...')
        self.temp_dir_ctl.del_folder_files()
        print('輸出檔案為' + result_name)

    @staticmethod
    def __load_todo_list():
        config_path = os.path.join('.', 'excel合併清單.xlsx')
        if not os.path.isfile(config_path):
            print('找不到：' + os.path.abspath(os.path.join('.', 'excel合併清單.xlsx')))
            return []
        config_df = pandas.read_excel(os.path.join('.', 'excel合併清單.xlsx'), dtype={'執行檔名': str})
        todo_list = config_df['執行檔名'].tolist()
        return todo_list

    @staticmethod
    def get_sheet1_a1(excel_path):
        if excel_path.endswith('xls'):
            t_wb = xlrd.open_workbook(excel_path)
            t_ws = t_wb.sheet_by_index(0)
            return t_ws.cell_value(0, 0)
        elif excel_path.endswith('xlsx'):
            t_wb = openpyxl.load_workbook(excel_path)
            t_ws = t_wb[t_wb.sheetnames[0]]
            return t_ws['A1'].value
